import { Router } from 'express';
import {
	findAllHotels, addHotel, findHotelById, updateHotel,
	deleteHotel
} from '../Controller/hoteles';

export function Routers(router: Router) {
	console.log('ROUTERS');
	router.get('/hotels', findAllHotels);
	router.post('/hotels', addHotel);

	router.get('/hotel/:id', findHotelById);
	router.get('/hotels/:name', findAllHotels);
	router.put('/hotels/:id', updateHotel);
	router.delete('/hotel/', deleteHotel);

	return router
}