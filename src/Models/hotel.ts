// import mongoose from 'mongoose';
import { Schema, Model, model, Document } from 'mongoose';

export const hotelSchema: Schema = new Schema({
	name: {type: String},
	stars: {type: Number},
	images: {type: String},
	price: {type: Number},
	address: {type: String},
	services: {type: Array}
}, { collection: "Hoteles"});

export interface Hoteles{
    name?: string
    stars?: number
    images?: string
    price?: number
    address?: string,
	services?: Array<string>
	
}
export interface HotelesModel extends Hoteles, Document {}

export var Hotel: Model<HotelesModel> = model<HotelesModel>("Hotel",hotelSchema);