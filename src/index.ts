import * as express from 'express';
import * as mongodb from 'mongoose';
import { Routers } from './Router/routers';
import * as bodyParse from 'body-parser';
import {CrearPrimerosHoteres} from "./Controller/HotelesIniciales";


let app = express();
let uri = 'mongodb://talomontes:Almundo17@ds249545.mlab.com:49545/almundo';
let mongoose = mongodb;
mongoose.Promise = global.Promise;
var router = express.Router();

app.use(bodyParse.urlencoded({ extended: false }));
app.use(bodyParse.json());

app.use("/", Routers(router));

mongoose.connect(uri, { useMongoClient: true });

let db = mongoose.connection;

db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => {
	app.listen(3000, function() {
		CrearPrimerosHoteres();
		console.log("Servidor corriendo en http://localhost:3000");
	});
});


