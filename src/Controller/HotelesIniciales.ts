import { Hotel, Hoteles } from '../Models/hotel';
// import HotelController from '../Controller/hoteles';

let ListaHoteles = [{
	"__v": 0,
	"name": "Hotel Cordoba 2",
	"stars": 5,
	"images": "https://fotos.hoteles.net/fotos/ficgra/hotel-eurostars-ciudad-de-cordoba-1.jpg",
	"price": 250,
	"address": "cra 25 #18 a",
	"services": ["wifi", "cutlery", "car", "paw", "coffee", "glass"]
},
{
	"__v": 0,
	"name": "Hotel  EuroStars",
	"stars": 3,
	"images": "https://media.expedia.com/hotels/2000000/1480000/1479000/1478995/1478995_20_b.jpg",
	"price": 300,
	"address": "cra 25 #18 a",
	"services":	["wifi","car","paw","glass"]
}]
export function CrearPrimerosHoteres() {
	Hotel.find().count().then(response => {
		if (response != 0) {
			return;
		}
		for (let i = 0; i < ListaHoteles.length; i++) {
			let hotel: Hoteles = {
				name: ListaHoteles[i].name,
				stars: ListaHoteles[i].stars,
				images: ListaHoteles[i].images,
				price: ListaHoteles[i].price,
				address: ListaHoteles[i].address,
				services: ListaHoteles[i].services
			};
			new Hotel(hotel).save();
		}
	});
};