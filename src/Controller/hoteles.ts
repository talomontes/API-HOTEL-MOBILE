// import Mongoose from 'mongoose';
import { Hotel } from '../Models/hotel';

export function findAllHotels(req:any, res:any) {
	let name = req.params.name;
	if(typeof name!='undefined'){
		Hotel.find({"name" : { $regex: name, $options: 'i' }},function(error, hotels) {
			if (error) res.send(500, error.message);
			console.log('Obteniendo hoteles por nombre name');
			res.status(200).json(hotels);
		});
	}else{
		Hotel.find(function(error, hotels) {
			if (error) res.send(500, error.message);
			console.log('Obteniendo todos los hoteles');
			res.status(200).json(hotels);
		});
	}
	
}


export function findHotelsCount() {
	return Hotel.find().count().exec(function(error, hotels) {
		return hotels;
	});
};


//get hotel by id
export function findHotelById(req:any, res:any) {
	Hotel.findById(req.params.id, function(error, hotel) {
		if (error) return res.send(500, error.message);

		console.log('Obteniendo el hotel con id:' + req.params.id);
		res.status(200).json(hotel);
	});
};

//Post -insert a Hotel
export function addHotel(req:any, res:any) {
	console.log('Insertando un hotel');
	var hotel = new Hotel({
		name: req.body.name,
		stars: req.body.stars,
		images: req.body.images,
		price: req.body.price,
		address: req.body.address,
		services: req.body.services
	});

	hotel.save(function(error, hotel) {
		if (error) return res.status(500).send(error.message);
		res.status(200).json(hotel);
	});
};

// PUT actualizar por id
export function updateHotel(req:any, res:any) {
	console.log('Actualizando un hotel');
	Hotel.findById(req.params.id, function(error, hotel) {
		hotel.name = req.body.name,
			hotel.stars = req.body.stars,
			hotel.images = req.body.images,
			hotel.price = req.body.price,
			hotel.address = req.body.address,
			hotel.services = req.body.services

		hotel.save(function(error) {
			if (error) return res.status(500).send(error.message);
			res.status(200).json(hotel);
		});
	});
};

//Delete

export function deleteHotel(req: any, res: any) {
	console.log('Eliminando el hotel: '+req.params.id);
	Hotel.findById(req.params.id, function(error, hotel) {
		hotel.remove(function(error) {
			if (error) return res.status(500).send(error.message);
			res.status(200).send();
		})
	});
};