const gulp = require('gulp');
const ts = require('gulp-typescript');
var tsProject = ts.createProject("tsconfig.json");
var sourcemaps = require('gulp-sourcemaps');
var nodemon = require('gulp-nodemon');
var clean = require('gulp-clean');
const babel = require('gulp-babel');
const JSON_FILES = ['src/*.json', 'src/**/*.json'];

gulp.task('compile', () => {
  const tsResult = tsProject.src()
  .pipe(tsProject()).js
  return tsResult.pipe(gulp.dest('app'));
});
gulp.task('watch', ['compile'], () => {
  gulp.watch('src/**/*.ts', ['compile']);
});
gulp.task('json-copy', function () {
  return gulp.src(JSON_FILES)
    .pipe(gulp.dest('app'));
});
gulp.task("clean", function () {
  return gulp.src(['app']).pipe(clean());
});
gulp.task('serve', ['compile'], function () {
  nodemon({
    exec: 'node index.js',
    watch: 'src',
    ext: 'ts',
    tasks: ['compile']
  });
});
gulp.task('default', ['watch', 'json-copy', 'serve']);