"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.hotelSchema = new mongoose_1.Schema({
    name: { type: String },
    stars: { type: Number },
    images: { type: String },
    price: { type: Number },
    address: { type: String },
    petFriendy: { type: Boolean }
}, { collection: "Hoteles" });
exports.Hotel = mongoose_1.model("Hotel", exports.hotelSchema);
