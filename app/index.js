"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mongodb = require("mongoose");
const routers_1 = require("./Router/routers");
const bodyParse = require("body-parser");
const HotelesIniciales_1 = require("./Controller/HotelesIniciales");
let app = express();
let uri = 'mongodb://talomontes:Almundo17@ds249545.mlab.com:49545/almundo';
let mongoose = mongodb;
mongoose.Promise = global.Promise;
var router = express.Router();
app.use(bodyParse.urlencoded({ extended: false }));
app.use(bodyParse.json());
app.use("/", routers_1.Routers(router));
mongoose.connect(uri, { useMongoClient: true });
let db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'));
db.once("open", () => {
    app.listen(3000, function () {
        HotelesIniciales_1.CrearPrimerosHoteres();
        console.log("Servidor corriendo en http://localhost:3000");
    });
});
