"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hoteles_1 = require("../Controller/hoteles");
function Routers(router) {
    console.log('ROUTERS');
    router.get('/hotels', hoteles_1.findAllHotels);
    router.post('/hotels', hoteles_1.addHotel);
    router.get('/hotel/:id', hoteles_1.findHotelById);
    router.get('/hotels/:name', hoteles_1.findAllHotels);
    router.put('/hotels/:id', hoteles_1.updateHotel);
    router.delete('/hotel/', hoteles_1.deleteHotel);
    return router;
}
exports.Routers = Routers;
