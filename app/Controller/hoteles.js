"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hotel_1 = require("../Models/hotel");
function findAllHotels(req, res) {
    let name = req.params.name;
    if (typeof name != 'undefined') {
        hotel_1.Hotel.find({ "name": { $regex: name, $options: 'i' } }, function (error, hotels) {
            if (error)
                res.send(500, error.message);
            console.log('GET /hotels by name');
            res.status(200).json(hotels);
        });
    }
    else {
        hotel_1.Hotel.find(function (error, hotels) {
            if (error)
                res.send(500, error.message);
            console.log('GET /hotels');
            res.status(200).json(hotels);
        });
    }
}
exports.findAllHotels = findAllHotels;
function findHotelsCount() {
    return hotel_1.Hotel.find().count().exec(function (error, hotels) {
        return hotels;
    });
}
exports.findHotelsCount = findHotelsCount;
;
function findHotelById(req, res) {
    hotel_1.Hotel.findById(req.params.id, function (error, hotel) {
        if (error)
            return res.send(500, error.message);
        console.log('GET /hotel/' + req.params.id);
        res.status(200).json(hotel);
    });
}
exports.findHotelById = findHotelById;
;
function addHotel(req, res) {
    console.log('INSERTANDO');
    console.log(req.body);
    var hotel = new hotel_1.Hotel({
        name: req.body.name,
        stars: req.body.stars,
        images: req.body.images,
        price: req.body.price,
        address: req.body.address,
        petFriendly: req.body.petFriendly
    });
    hotel.save(function (error, hotel) {
        if (error)
            return res.status(500).send(error.message);
        res.status(200).json(hotel);
    });
}
exports.addHotel = addHotel;
;
function updateHotel(req, res) {
    hotel_1.Hotel.findById(req.params.id, function (error, hotel) {
        hotel.name = req.body.name,
            hotel.stars = req.body.stars,
            hotel.images = req.body.images,
            hotel.price = req.body.price,
            hotel.address = req.body.address,
            hotel.petFriendly = req.body.petFriendly;
        hotel.save(function (error) {
            if (error)
                return res.status(500).send(error.message);
            res.status(200).json(hotel);
        });
    });
}
exports.updateHotel = updateHotel;
;
function deleteHotel(req, res) {
    console.log('eliminandoooo ' + req.params.id);
    hotel_1.Hotel.findById(req.params.id, function (error, hotel) {
        hotel.remove(function (error) {
            if (error)
                return res.status(500).send(error.message);
            res.status(200).send();
        });
    });
}
exports.deleteHotel = deleteHotel;
;
