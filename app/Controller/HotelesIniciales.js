"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hotel_1 = require("../Models/hotel");
let ListaHoteles = [{
        "__v": 0,
        "name": "Paraiso 2",
        "stars": 5,
        "images": "http://ia.media-imdb.com/images/M/MV5BMjA3NzMyMzU1MV5BMl5BanBnXkFtZTcwNjc1ODUwMg@@._V1_SY317_CR17,0,214,317_.jpg",
        "price": 25,
        "address": "cra 25 #18 a",
        "petFriendly": false
    },
    {
        "__v": 0,
        "name": "Paraiso 1",
        "stars": 3,
        "images": "http://ia.media-imdb.com/images/M/MV5BMjA3NzMyMzU1MV5BMl5BanBnXkFtZTcwNjc1ODUwMg@@._V1_SY317_CR17,0,214,317_.jpg",
        "price": 3000,
        "address": "cra 25 #18 a",
        "petFriendly": true
    }];
function CrearPrimerosHoteres() {
    hotel_1.Hotel.find().count().then(response => {
        if (response != 0) {
            return;
        }
        for (let i = 0; i < ListaHoteles.length; i++) {
            let hotel = {
                name: ListaHoteles[i].name,
                stars: ListaHoteles[i].stars,
                images: ListaHoteles[i].images,
                price: ListaHoteles[i].price,
                address: ListaHoteles[i].address,
                petFriendly: ListaHoteles[i].petFriendly
            };
            new hotel_1.Hotel(hotel).save();
        }
    });
}
exports.CrearPrimerosHoteres = CrearPrimerosHoteres;
;
